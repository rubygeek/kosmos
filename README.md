# kosmos

kosmos -- greek for 'ordered system'

This library is an attempt to recreate a much weaker and simpler
version of Semperos' liftoff library. It is built on top of Stuart
Sierra's component library. The purpose is to bring an ordered system
into existence based on a simple configuration map and to be able to
reference the assembled components and dependencies at runtime.

## Usage


## License

Kosmos is distributed under the [Eclipse Public License](http://opensource.org/licenses/eclipse-1.0.php), the same as Clojure.
